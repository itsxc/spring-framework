package org.springframework.uml;

//学生类
class Person{}
//墙壁类
class Wall{}
//学校接口
interface School{}
//残障学校
class DisabilitySchool{}

//Classroom(教室)类与School(学校接口)接口是 TODO 实现关系
//当前类与DisabilitySchool(残障学校)类是 TODO 泛化(继承)关系
public class Classroom extends DisabilitySchool implements School {

	//教室类与学生类是 TODO 聚合关系
	//教室可以有学生，也可以没有。
	//教室是整体，学生是部分，整体与部分之间是可分离的，他们可以具有各自的生命周期，部分可以属于多个整体对象，也可以为多个整体对象共享。
	private Person[] persons;

	//教室类与墙壁类是 TODO 组合关系
	//组合同样体现整体与部分间的关系，但此时整体与部分是不可分的，整体的生命周期结束也就意味着部分的生命周期结束。
	//就像你有鼻子有眼睛，如果你一不小心结束了生命周期，鼻子和眼睛的生命周期也会结束，而且，鼻子和眼睛不能脱离你单独存在。
	//只看代码，你是无法区分关联，聚合和组合的，具体是哪一种关系，只能从语义级别来区分。
	private Wall wall;

	//当前类和 String 类是 TODO 关联关系
	//关联体现的是两个类、或者类与接口之间语义级别的一种强依赖关系。
	//这种关系比依赖更强、不存在依赖关系的偶然性、关系也不是临时性的，一般是长期性的，而且双方的关系一般是平等的、关联可以是单向、双向的。
	private String msg;

	// TODO 聚合关系、组合关系、关联关系
	//他们在代码上是看不出任何不同的，他们的不同点在于含义上的不同
	//组合代表我们是整体不可缺少(人和人的心脏的关系)
	//聚合代表我们是拥有关系，我拥有电脑，拥有耳机，但是如果电脑坏了，我还存在
	//关联代表我和你是朋友，但是我们没有拥有对方的肉体，你也不是我的一部分，我们是平等关系，关联可以是单向或是双向的
	//代表可能我觉得我们是朋友，但你不这么认为(单向)或者你也认为我们是朋友(双向)

	//当前类和 String 类是 TODO 依赖关系
	//所有在方法中引用到的其他对象那么都可以说是依赖关系
	public void test(String msg){
		String test;
		String.format(msg,"");
	}

}
