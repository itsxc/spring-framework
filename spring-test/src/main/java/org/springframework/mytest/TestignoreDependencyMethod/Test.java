package org.springframework.mytest.TestignoreDependencyMethod;

import org.springframework.beans.factory.BeanNameAware;

import java.util.List;

public interface Test  {

	void setTest(List<String> test);

}
