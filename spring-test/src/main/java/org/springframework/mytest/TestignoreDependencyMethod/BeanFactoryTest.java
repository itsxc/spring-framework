package org.springframework.mytest.TestignoreDependencyMethod;

import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

public class BeanFactoryTest {

	public static void main(String[] args) {

		/**
		 * 测试 ignoreDependencyInterface 方法是否可以做到忽略对应的接口实现类的自动装配
		 *
		 * ignoreDependencyInterface 方法作用是忽略给定接口的自动装配功能
		 * 使用场景,当我们在使用 BeanNameAware 时，我们需要重写其中的setBeanName方法
		 * 但是如果我们在定义Bean的时候将beanName的值在XML中定义好自动装配的话那么这个值实际上
		 * 和真正的Bean名称是不一样的，一个是我们定义的一个是Spring返回的，但是这样是不合理的
		 * 所以我们需要禁止把BeanName这个属性手动注入进来，只使用Spring传入的BeanName
		 *
		 * 使用 ignoreDependencyInterface(BeanNameAware.class); 的含义将会是禁止干涉BeanName的装配过程
		 *
		 * https://www.jianshu.com/p/3c7e0608ff1f  思路来源
		 */


		ClassPathResource classPathResource = new ClassPathResource("beanFactoryTest.xml");
		XmlBeanFactory beanFactory = new XmlBeanFactory(classPathResource);
		//忽略Test接口实现类的自动装配
		beanFactory.ignoreDependencyInterface(Test.class);
		TestImpl test = (TestImpl)beanFactory.getBean("testImpl");
		//null
		System.out.println(test.getTest());


	}

}
