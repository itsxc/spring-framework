package org.springframework.mytest.TestignoreDependencyMethod;

import java.util.List;

public class TestImpl implements Test {

	List<String> test;

	@Override
	public void setTest(List<String> test) {
		this.test = test;
	}

	public List<String> getTest() {
		return test;
	}
}
