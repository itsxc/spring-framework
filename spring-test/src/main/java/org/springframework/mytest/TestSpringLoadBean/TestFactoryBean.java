package org.springframework.mytest.TestSpringLoadBean;

import org.springframework.beans.factory.FactoryBean;

public class TestFactoryBean implements FactoryBean<String> {

	@Override
	public String getObject() throws Exception {
		return new String("史喜超");
	}

	@Override
	public Class<?> getObjectType() {
		return String.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}
}
