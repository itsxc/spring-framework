package org.springframework.mytest.TestSpringLoadBean;

import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mytest.TestLoopupMethod.Student;
import org.springframework.mytest.TestignoreDependencyMethod.Test;
import org.springframework.mytest.TestignoreDependencyMethod.TestImpl;

public class BeanFactoryTest {

	public static void main(String[] args) {
		//通过ClassPath读取Xml配置文件，并转换为Resource对象(Resource资源对象是Spring的资源封装类所有资源都会转成这个对象)
		ClassPathResource classPathResource = new ClassPathResource("beanFactoryTest.xml");
		//通过XmlBeanFactory解析Resource转换为Document然后根据Document解析为BeanDefinition存放到容器中
		XmlBeanFactory beanFactory = new XmlBeanFactory(classPathResource);
		Student bean = beanFactory.getBean("student1",Student.class,"史喜超1");
		bean.show();


	}

}
