package org.springframework.mytest.TestLoopupMethod;

public abstract class GetBeanTest {

	public void show(){
		getBean().show();
	}

	public abstract User getBean();

}
