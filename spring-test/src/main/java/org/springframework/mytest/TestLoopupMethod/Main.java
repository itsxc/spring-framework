package org.springframework.mytest.TestLoopupMethod;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {

		ApplicationContext bf = new ClassPathXmlApplicationContext("beanFactoryTest.xml");
		GetBeanTest getBeanTest = (GetBeanTest)bf.getBean("getBeanTest");
		getBeanTest.show();
	}
}
