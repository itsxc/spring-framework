package org.springframework.mytest.TestLoopupMethod;

public class Student extends User {

	private String msg;

	public Student() {
	}

	public Student(String msg) {
		this.msg = msg;
	}



	@Override
	public void show() {
		System.out.println(msg);
	}
}
