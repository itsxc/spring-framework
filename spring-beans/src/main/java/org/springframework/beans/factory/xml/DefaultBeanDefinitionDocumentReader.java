/*
 * Copyright 2002-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.beans.factory.xml;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.springframework.beans.factory.BeanDefinitionStoreException;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.parsing.BeanComponentDefinition;
import org.springframework.beans.factory.support.BeanDefinitionReaderUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;

/**
 * Default implementation of the {@link BeanDefinitionDocumentReader} interface that
 * reads bean definitions according to the "spring-beans" DTD and XSD format
 * (Spring's default XML bean definition format).
 *
 * <p>The structure, elements, and attribute names of the required XML document
 * are hard-coded in this class. (Of course a transform could be run if necessary
 * to produce this format). {@code <beans>} does not need to be the root
 * element of the XML document: this class will parse all bean definition elements
 * in the XML file, regardless of the actual root element.
 *
 * @author Rod Johnson
 * @author Juergen Hoeller
 * @author Rob Harrop
 * @author Erik Wiersma
 * @since 18.12.2003
 */
public class DefaultBeanDefinitionDocumentReader implements BeanDefinitionDocumentReader {

	public static final String BEAN_ELEMENT = BeanDefinitionParserDelegate.BEAN_ELEMENT;

	public static final String NESTED_BEANS_ELEMENT = "beans";

	public static final String ALIAS_ELEMENT = "alias";

	public static final String NAME_ATTRIBUTE = "name";

	public static final String ALIAS_ATTRIBUTE = "alias";

	public static final String IMPORT_ELEMENT = "import";

	public static final String RESOURCE_ATTRIBUTE = "resource";

	public static final String PROFILE_ATTRIBUTE = "profile";


	protected final Log logger = LogFactory.getLog(getClass());

	@Nullable
	private XmlReaderContext readerContext;

	//解析元素的类，根据标签解析成Bean
	@Nullable
	private BeanDefinitionParserDelegate delegate;


	/**
	 * This implementation parses bean definitions according to the "spring-beans" XSD
	 * (or DTD, historically).
	 * <p>Opens a DOM Document; then initializes the default settings
	 * specified at the {@code <beans/>} level; then parses the contained bean definitions.
	 */
	@Override
	public void registerBeanDefinitions(Document doc, XmlReaderContext readerContext) {
		this.readerContext = readerContext;
		//核心注册逻辑底层,这里开始真正的解析Bean了
		doRegisterBeanDefinitions(doc.getDocumentElement());
	}

	/**
	 * Return the descriptor for the XML resource that this parser works on.
	 */
	protected final XmlReaderContext getReaderContext() {
		Assert.state(this.readerContext != null, "No XmlReaderContext available");
		return this.readerContext;
	}

	/**
	 * Invoke the {@link org.springframework.beans.factory.parsing.SourceExtractor}
	 * to pull the source metadata from the supplied {@link Element}.
	 */
	@Nullable
	protected Object extractSource(Element ele) {
		return getReaderContext().extractSource(ele);
	}


	/**
	 * Register each bean definition within the given root {@code <beans/>} element.
	 *
	 * TODO 处理beans元素
	 */
	@SuppressWarnings("deprecation")  // for Environment.acceptsProfiles(String...)
	protected void doRegisterBeanDefinitions(Element root) {
		// Any nested <beans> elements will cause recursion in this method. In
		// order to propagate and preserve <beans> default-* attributes correctly,
		// keep track of the current (parent) delegate, which may be null. Create
		// the new (child) delegate with a reference to the parent for fallback purposes,
		// then ultimately reset this.delegate back to its original (parent) reference.
		// this behavior emulates a stack of delegates without actually necessitating one.

		/**
		 * 上面的含义是说如果存在签到的beans标签就会产生递归
		 * <beans>
		 *     <beans></beans>   [案例1]
		 * </beans>
		 * BeanDefinitionParserDelegate 是 BeanDefinition 解析器
		 * this.delegate 存放的是当前正在解析的元素信息
		 *
		 * 但是我们发现 this.delegate 没有赋值，这个值代表的是解析的父元素的解析器对象，
		 * [案例1]这种情况下 parent 就是父元素的解析器对象
		 *
		 * 当没有父元素时为null，存在父元素时 parent 就是父元素的解析器
		 *
		*/
		BeanDefinitionParserDelegate parent = this.delegate;

		/**
		 * 为当前元素创建解析器对象，解析元素将通过 BeanDefinitionParserDelegate 来实现
		 * 并解析当前 beans 标签的属性
		*/
		this.delegate = createDelegate(getReaderContext(), root, parent);

		//如果当前解析的元素是默认的命名空间
		if (this.delegate.isDefaultNamespace(root)) {

			//项目环境例如 dev master 等 <beans profile='dev' ></beans>
			String profileSpec = root.getAttribute(PROFILE_ATTRIBUTE);

			//如果环境属性存在
			if (StringUtils.hasText(profileSpec)) {
				//将环境值根据逗号分号分割
				String[] specifiedProfiles = StringUtils.tokenizeToStringArray(
						profileSpec, BeanDefinitionParserDelegate.MULTI_VALUE_ATTRIBUTE_DELIMITERS);

				// We cannot use Profiles.of(...) since profile expressions are not supported
				// in XML config. See SPR-12458 for details.
				//如果当前使用的环境不是这个环境的话就return
				if (!getReaderContext().getEnvironment().acceptsProfiles(specifiedProfiles)) {
					if (logger.isDebugEnabled()) {
						logger.debug("Skipped XML bean definition file due to specified profiles [" + profileSpec +
								"] not matching: " + getReaderContext().getResource());
					}
					return;
				}
			}
		}

		//解析前处理，留给子类实现，模板方法模式，和我们写的意思一样
		preProcessXml(root);

		/**
		 * 解析当前 beans 标签下的所有子标签并注册 BeanDefinitions
		 * root 是当前解析的元素Document
		 * delegate 是当前beans元素的解析信息
		*/
		parseBeanDefinitions(root, this.delegate);

		//解析后处理
		postProcessXml(root);

		//将当前的解析器设置为parent，这里不理解查看方法开始时的注解
		this.delegate = parent;
	}

	/**
	 *
	 * 创建一个解析器并返回
	 *
	*/
	protected BeanDefinitionParserDelegate createDelegate(
			XmlReaderContext readerContext, Element root, @Nullable BeanDefinitionParserDelegate parentDelegate) {

		BeanDefinitionParserDelegate delegate = new BeanDefinitionParserDelegate(readerContext);

		//解析beans下的属性
		delegate.initDefaults(root, parentDelegate);
		return delegate;
	}

	/**
	 * Parse the elements at the root level in the document:
	 * "import", "alias", "bean".
	 *
	 * 解析beans元素下的所有子元素 "import", "alias", "bean".
	 *
	 * @param root the DOM root element of the document
	 */
	protected void parseBeanDefinitions(Element root, BeanDefinitionParserDelegate delegate) {

		/**
		 * 查看当前操作的标签是否是默认校验文件的标签
		 * 比方说spring的配置文件中不允许出现sadw标签(瞎写的根本不存在)这个方法就是判断你这个标签是不是spring的标签
		*/
		if (delegate.isDefaultNamespace(root)) {
			//获得所有子节点 <beans> <子节点></子节点> </beans>
			NodeList nl = root.getChildNodes();

			/**
			 *  这里开始解析所有子元素
			*/
			for (int i = 0; i < nl.getLength(); i++) {

				//得到元素实例
				Node node = nl.item(i);
				if (node instanceof Element) {
					Element ele = (Element) node;

					//如果是Spring自己的标签就这么解析
					if (delegate.isDefaultNamespace(ele)) {
						parseDefaultElement(ele, delegate);
					}

					/**
					 * TODO[注释一号准备就绪]
					 * 如果是自定义的命名空间就根据相应的命名空间解析器来解析当前元素
					 * 我们可以自定义命名空间来实现自定义元素
					 * 然后可以 实现 NamespaceHandler 接口 定义我们自己的元素解析器
					 * @see NamespaceHandler 元素解析器
					 *
					 * 你不知道是怎么将元素解析器和命名空间对应上的？ 看下面的类
					 * @see DefaultNamespaceHandlerResolver#resolve(String)  请看此方法
					 * 当使用到自定义元素解析器时都会通过上述方法来获取对应的元素解析器并调用解析器中对应的方法
					*/
					else {
						delegate.parseCustomElement(ele);
					}
				}
			}
		}
		// 请查看 TODO[注释一号准备就绪]
		else {
			delegate.parseCustomElement(root);
		}
	}

	/**
	 * 解析默认的元素标签，如<beans/>,<bean />,<import />,<alias />，每一种标签都有对应的方法。
	 * 这里判断当前的子元素是什么元素，然后根据元素类型得到相应的解析
	*/
	private void parseDefaultElement(Element ele, BeanDefinitionParserDelegate delegate) {

		/**
		 *  解析 import 标签
		 *  标签讲解： https://www.cnblogs.com/javahr/p/8384681.html
		 *  作用大概就是可以将其他资源文件引入
		 *  引入类路径下 /spring/job-timer.xml 文件
		 *  <import resource="classpath*:/spring/job-timer.xml" />
		*/
		if (delegate.nodeNameEquals(ele, IMPORT_ELEMENT)) {
			importBeanDefinitionResource(ele);
		}

		/**
		 *  解析 alias 标签
		 *  标签讲解： https://blog.csdn.net/truong/article/details/27837699
		 *  作用大概就是可以为 bean 定义一个别名，这个别名可以作为 bean 的引用一样
		 *  将 beanName 为 componentA 的 bean 定义一个别名为 componentB
		 *  <alias name="componentA" alias="componentB"/>
		*/
		else if (delegate.nodeNameEquals(ele, ALIAS_ELEMENT)) {
			processAliasRegistration(ele);
		}

		/**
		 *  解析 bean 标签
		 *  标签讲解：https://blog.csdn.net/ZixiangLi/article/details/87937819
		 *  作用就是可以向 ioc 容器中定义一个 bean
		 *  定义一个 id 为 teacher 类为 Teacher 的类
		 *  <bean id="teacher" class="org.springframework.mytest.TestLoopupMethod.Teacher"></bean>
		*/
		else if (delegate.nodeNameEquals(ele, BEAN_ELEMENT)) {
			processBeanDefinition(ele, delegate);
		}

		/**
		 *  解析 beans 元素
		 *  标签讲解： https://blog.csdn.net/xiao1_1bing/article/details/81082938
		 *  类似于 bean 元素的容器一样，这个元素会递归调用 {@link DefaultBeanDefinitionDocumentReader#doRegisterBeanDefinitions} 方法
		*/
		else if (delegate.nodeNameEquals(ele, NESTED_BEANS_ELEMENT)) {
			// 这里就是刚才一堆英文中所说递归的地方
			doRegisterBeanDefinitions(ele);
		}
	}

	/**
	 * Parse an "import" element and load the bean definitions
	 * from the given resource into the bean factory.
	 *
	 * 解析一个“ import”元素，并将给定资源中的bean定义*加载到bean工厂中。
	 */
	protected void importBeanDefinitionResource(Element ele) {
		String location = ele.getAttribute(RESOURCE_ATTRIBUTE);
		if (!StringUtils.hasText(location)) {
			getReaderContext().error("Resource location must not be empty", ele);
			return;
		}

		// Resolve system properties: e.g. "${user.dir}"
		location = getReaderContext().getEnvironment().resolveRequiredPlaceholders(location);

		Set<Resource> actualResources = new LinkedHashSet<>(4);

		// Discover whether the location is an absolute or relative URI
		boolean absoluteLocation = false;
		try {
			absoluteLocation = ResourcePatternUtils.isUrl(location) || ResourceUtils.toURI(location).isAbsolute();
		}
		catch (URISyntaxException ex) {
			// cannot convert to an URI, considering the location relative
			// unless it is the well-known Spring prefix "classpath*:"
		}

		// Absolute or relative?
		if (absoluteLocation) {
			try {
				int importCount = getReaderContext().getReader().loadBeanDefinitions(location, actualResources);
				if (logger.isTraceEnabled()) {
					logger.trace("Imported " + importCount + " bean definitions from URL location [" + location + "]");
				}
			}
			catch (BeanDefinitionStoreException ex) {
				getReaderContext().error(
						"Failed to import bean definitions from URL location [" + location + "]", ele, ex);
			}
		}
		else {
			// No URL -> considering resource location as relative to the current file.
			try {
				int importCount;
				Resource relativeResource = getReaderContext().getResource().createRelative(location);
				if (relativeResource.exists()) {
					importCount = getReaderContext().getReader().loadBeanDefinitions(relativeResource);
					actualResources.add(relativeResource);
				}
				else {
					String baseLocation = getReaderContext().getResource().getURL().toString();
					importCount = getReaderContext().getReader().loadBeanDefinitions(
							StringUtils.applyRelativePath(baseLocation, location), actualResources);
				}
				if (logger.isTraceEnabled()) {
					logger.trace("Imported " + importCount + " bean definitions from relative location [" + location + "]");
				}
			}
			catch (IOException ex) {
				getReaderContext().error("Failed to resolve current resource location", ele, ex);
			}
			catch (BeanDefinitionStoreException ex) {
				getReaderContext().error(
						"Failed to import bean definitions from relative location [" + location + "]", ele, ex);
			}
		}
		Resource[] actResArray = actualResources.toArray(new Resource[0]);
		getReaderContext().fireImportProcessed(location, actResArray, extractSource(ele));
	}

	/**
	 * Process the given alias element, registering the alias with the registry.
	 *
	 * 处理给定的alias元素，并在注册表中注册别名。
	 *
	 */
	protected void processAliasRegistration(Element ele) {

		//得到Bean的名称和定义的别名
		String name = ele.getAttribute(NAME_ATTRIBUTE);
		String alias = ele.getAttribute(ALIAS_ATTRIBUTE);


		boolean valid = true;
		if (!StringUtils.hasText(name)) {
			getReaderContext().error("Name must not be empty", ele);
			valid = false;
		}
		if (!StringUtils.hasText(alias)) {
			getReaderContext().error("Alias must not be empty", ele);
			valid = false;
		}
		if (valid) {
			try {
				getReaderContext().getRegistry().registerAlias(name, alias);
			}
			catch (Exception ex) {
				getReaderContext().error("Failed to register alias '" + alias +
						"' for bean with name '" + name + "'", ele, ex);
			}
			getReaderContext().fireAliasRegistered(name, alias, extractSource(ele));
		}
	}

	/**
	 * Process the given bean element, parsing the bean definition
	 * and registering it with the registry.
	 *
	 * 处理给定的bean元素，解析bean定义并将其注册到注册表中。
	 *
	 */
	protected void processBeanDefinition(Element ele, BeanDefinitionParserDelegate delegate) {

		//TODO 解析bean标签元素 返回BeanDefinitionHolder(返回BeanDefinition 持有人)
		BeanDefinitionHolder bdHolder = delegate.parseBeanDefinitionElement(ele);

		if (bdHolder != null) {
			//根据命名空间对 BeanDefinitionHolder 装饰
			bdHolder = delegate.decorateBeanDefinitionIfRequired(ele, bdHolder);
			try {
				// Register the final decorated instance.
				//注册BeanDefinition  终于要注册了好苦啊
				//registerBeanDefinition(BeanDefinitionHolder, BeanDefinitionRegistry)
				//   BeanDefinitionHolder BeanDefinition的持有者
				//   BeanDefinitionRegistry: 定义对BeanDefinition的各种增删改查操作(向容器中增加一个Bean等等)相当于我们查询数据库实体的Service对象
				BeanDefinitionReaderUtils.registerBeanDefinition(bdHolder, getReaderContext().getRegistry());
			}
			catch (BeanDefinitionStoreException ex) {
				getReaderContext().error("Failed to register bean definition with name '" +
						bdHolder.getBeanName() + "'", ele, ex);
			}
			// Send registration event.
			/**
			 *
			 * 通知监听器解析成功既注册完成
			 * 当我们注册监听器后BeanDefinition注册成功会回调到我们的监听器执行自定义逻辑
			 * 现在没有Spring没有提供这个功能
			 *
			 * @see EmptyReaderEventListener 事件触发器,现在里面啥代码都没有
			 * 这个事件触发器是在
			 * @see XmlBeanDefinitionReader#registerBeanDefinitions 方法中TODO标注的地方定义的
			*/
			getReaderContext().fireComponentRegistered(new BeanComponentDefinition(bdHolder));
		}
	}


	/**
	 * Allow the XML to be extensible by processing any custom element types first,
	 * before we start to process the bean definitions. This method is a natural
	 * extension point for any other custom pre-processing of the XML.
	 * <p>The default implementation is empty. Subclasses can override this method to
	 * convert custom elements into standard Spring bean definitions, for example.
	 * Implementors have access to the parser's bean definition reader and the
	 * underlying XML resource, through the corresponding accessors.
	 * @see #getReaderContext()
	 */
	protected void preProcessXml(Element root) {
	}

	/**
	 * Allow the XML to be extensible by processing any custom element types last,
	 * after we finished processing the bean definitions. This method is a natural
	 * extension point for any other custom post-processing of the XML.
	 * <p>The default implementation is empty. Subclasses can override this method to
	 * convert custom elements into standard Spring bean definitions, for example.
	 * Implementors have access to the parser's bean definition reader and the
	 * underlying XML resource, through the corresponding accessors.
	 * @see #getReaderContext()
	 */
	protected void postProcessXml(Element root) {
	}

}
